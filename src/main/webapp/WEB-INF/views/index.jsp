<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<link rel="stylesheet" href="/resources/css/styles.css">
<title>Welcome Page</title>
</head>
<body>
	<h1>${msg}</h1>

	<h2>
		Hoy es:
		<fmt:formatDate value="${today}" pattern="dd-MM-yyy" />
	</h2>
	<img src="/resources/imgs/gitlab.png" alt="Image" class="center">
	<p>Este es un ejemplo de un p&aacute;rrafo.</p>
	<p>
		Todo lo que se encuentra dentro de la etiqueta <strong>body</strong> aparecer&aacute; en el navegador.
	</p>
</body>
</html>